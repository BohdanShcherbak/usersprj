﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using DAL.Model;
using DAL;

namespace SmallProject.Controllers
{
    public class HomeController : Controller
    {


        private IRepository userRepository = new Repository();

        [HttpGet]
        public JsonResult GetAllUsers()
        {
            var data = userRepository.GetAllUsers();
            return new JsonResult() { Data = data, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public JsonResult CreateUsers(User user)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    userRepository.AddUser(user);
                    userRepository.Save();

                    return Json(new { redirectUrl = Url.Action("Index", "Home") });
                }
                catch (Exception ex)
                {
                    ViewBag.Error = "Error" + ex;
                }
            }
            return Json(new
            {
                errors = ModelState.Keys.SelectMany(i => ModelState[i].Errors).Select(m => m.ErrorMessage).ToArray()
            });
        }

        [HttpGet]
        public JsonResult GetUserById(int UserId)
        {
            var user = userRepository.GetUserById(UserId);
            
            return new JsonResult() { Data = user, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        //[HttpGet]
        //public JsonResult GetUserSection(int UserId)
        //{
        //    //var product = productRepository.GetProductById(ProductId);
        //    TestContext context = new TestContext();
        //    var getProd = productRepository.GetProductById(ProductId);
        //    var count = context.WareHouses.Where(s => s.ProductId == ProductId).Count();
        //    var total = context.WareHouses.Where(s => s.ProductId == ProductId).Sum(s => s.Product.Price);

        //    SectionViewModel product = new SectionViewModel()
        //    {
        //        Price = getProd.Price,
        //        Title = getProd.Title,
        //        Count = count,
        //        Total = total
        //    };

        //    return new JsonResult() { Data = product, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        //}

        [HttpPost]
        public string EditProduct(User users)
        {
            userRepository.UpdateUser(users);
            userRepository.Save();

            return "Data successfully added";
        }


        //[ValidateAntiForgeryToken]
        public string DeleteUser(int UserId)
        {
            if (ModelState.IsValid)
            {
                userRepository.DeleteUser(UserId);
                userRepository.Save();
                return "Данные удалены!";
            }
            else
            {
                ModelState.AddModelError("", "Error");
                return "Что-то пошло не так!";
            }
        }





        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}