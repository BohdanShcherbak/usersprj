﻿myapp.controller('crudcontroller', function ($scope, crudservice, uiGridConstants) {

    var Users = crudservice.getAllUsers();

    $scope.Users;
    $scope.Users = {}; 
    $scope.gridOptions = {
        enableCellEditOnFocus: true,
        enableColumnResizing: true,
        enableGridMenu: true,
        showColumnFooter: true,
        enablePaginationControls: true,
        enableRowSelection: true,
        onRegisterApi: function (gridApi) { $scope.gridApi = gridApi; },
        paginationPageSizes: [10, 25, 50, 100],
        paginationPageSize: 10,

        importerDataAddCallback: function importerDataAddCallback(grid, newObjects) {
            $scope.myData = $scope.data.concat(newObjects);
        },
        columnDefs: [

            { field: 'UserName' },
            { field: 'Login'},
            { field: 'Password' }

        ],

    };

    //show all users
    loadUserss();
    function loadUserss() {
        var Users = crudservice.getAllUsers();
        Users.then(function (data) {
            $scope.Users = data.data;
            $scope.gridOptions.data = $scope.Users;
           // $scope.gridApi.core.refresh();

        },
            function () {
                console("Oops..", "Error occured while loading", "error");
            });
    }


        


});