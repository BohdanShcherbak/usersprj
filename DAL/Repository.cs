﻿using System;
using DAL.Model;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace DAL
{
    public class Repository : IRepository
    {
        private DB context;
        public Repository()
        {
            context = new DB();
        }

        public void AddUser(User user)
        {
            context.Users.Add(user);
          
        }
        public void DeleteUser(int UserId)
        {
            var getUser = context.Users.Find(UserId);
            context.Users.Remove(getUser);
        }
        public IEnumerable<User> GetAllUsers()
        {
            var data = context.Users.ToList();
            return data;
        }
        public User GetUserById(int UserId)
        {
            return context.Users.Find(UserId);
        }
        public void Save()
        {
            context.SaveChanges();
        }
        public void UpdateUser(User user)
        {
            context.Entry(user).State = EntityState.Modified;
        }
    }
}
