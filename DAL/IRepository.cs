﻿using DAL.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public interface IRepository
    {
        IEnumerable<User> GetAllUsers();
        User GetUserById(int UserId);
        void DeleteUser(int UserId);
        void AddUser(User user);
        void UpdateUser(User user);
        void Save();
    }
}
